package com.automation.utils;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class WaitWebDriver {

  public WaitWebDriver() {
    super();
  }

  public static void waitVisibility(WebDriver driver, WebElement element, int time) {
    WebDriverWait wait = new WebDriverWait(driver, time);
    wait.until(ExpectedConditions.visibilityOf(element));
  }

  public static void waitClickable(WebDriver driver, WebElement element, int time) {
    WebDriverWait wait = new WebDriverWait(driver, time);
    wait.until(ExpectedConditions.elementToBeClickable(element));
  }

  public static void waitSelected(WebDriver driver, WebElement element, int time) {
    WebDriverWait wait = new WebDriverWait(driver, time);
    wait.until(ExpectedConditions.elementToBeSelected(element));
  }

  public static void waitAlert(WebDriver driver, int time) {
    WebDriverWait wait = new WebDriverWait(driver, time);
    wait.until(ExpectedConditions.alertIsPresent());
  }

  public static void waitInvisibility(WebDriver driver, WebElement element, int time) {
    WebDriverWait wait = new WebDriverWait(driver, time);
    wait.until(ExpectedConditions.invisibilityOf(element));
  }

  public static void waitVisibilityLocator(WebDriver driver, String xpath, int time) {
    WebDriverWait wait = new WebDriverWait(driver, time);
    wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath(xpath)));
  }
  
  public static void waitSeconds(int seconds) {
	  try {
		  Thread.sleep(seconds * 1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}
