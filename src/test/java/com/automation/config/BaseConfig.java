package com.automation.config;

import java.lang.reflect.Method;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

public class BaseConfig {
	public WebDriver driver;
	public String baseURL = "https://buggy.justtestit.org/";
	String driverPath = System.getProperty("user.dir") + "/src/test/resources/drivers/mac/chromedriver";

	@BeforeMethod(alwaysRun = true)
	public void beforeMethodSetup(Method method) throws Exception {
		String exePath = driverPath;
		System.setProperty("webdriver.chrome.driver", exePath);
		driver = new ChromeDriver();
		try {
			driver.manage().deleteAllCookies();
			driver.get(baseURL);
		} catch (Exception e) {
			System.out.println(e.toString());
		}
	}

	@AfterMethod(alwaysRun = true)
	public void afterMethodSetup(ITestResult result) {
		try {
			driver.close();
			driver.quit();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

}
