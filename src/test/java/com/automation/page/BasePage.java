package com.automation.page;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import com.automation.config.BaseConfig;

public class BasePage extends BaseConfig{
	public WebDriver driver;

	  public BasePage(WebDriver driver) {
	    PageFactory.initElements(driver, this);
	    this.driver = driver;
	  }
	
	

}
