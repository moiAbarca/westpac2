package com.automation.page;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import com.automation.utils.WaitWebDriver;

public class HomePage extends BasePage {

	public HomePage(WebDriver driver) {
		super(driver);
		PageFactory.initElements(driver, this);
	}

	@FindBy(name = "login")
	private WebElement txtLogin;

	@FindBy(xpath = "//button[@type='submit']")
	private WebElement btnLogin;

	@FindBy(name = "password")
	private WebElement txtPassword;

	@FindBy(xpath = "//a[text()='Profile']")
	private WebElement btnProfile;
	
	@FindBy(xpath = "(//img[@class=\"img-fluid center-block\"])[1]")
	private WebElement imgPopularMake;
	
	@FindBy(xpath = "//h2[text()='Popular Make']")
	private WebElement lblPopularMake;

	public void login() {
		try {
			WaitWebDriver.waitClickable(driver, btnLogin, 10);
			txtLogin.sendKeys("hitokiri");
			txtPassword.sendKeys("Asd123456@");
			btnLogin.click();
		} catch (Exception e) {
			System.out.println(e.getMessage().toString());
		}
	}

	public void enterProfile() {
		try {
			WaitWebDriver.waitClickable(driver, btnProfile, 10);
			btnProfile.click();
		} catch (Exception e) {
			System.out.println(e.getMessage().toString());
		}
	}

	public ProfilePage profilePage() {
		ProfilePage profilePage = new ProfilePage(driver);
		return profilePage;
	}
	
	public void popularMake() {
		try {
			WaitWebDriver.waitClickable(driver, imgPopularMake, 10);
			imgPopularMake.click();
		} catch (Exception e) {
			System.out.println(e.getMessage().toString());
		}
	}
	
	public PopularMakePage popularMakePage() {
		PopularMakePage popularMakePage = new PopularMakePage(driver);
		return popularMakePage;
	}
	
	public boolean validHomePage() {
		boolean bolean = false;
		try {
			WaitWebDriver.waitInvisibility(driver, lblPopularMake, 10);
			bolean = true;
		} catch (Exception e) {
			System.out.println(e.getMessage().toString());
		}
		return bolean;
	}
	
	
	
	

}
