package com.automation.page;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import com.automation.utils.WaitWebDriver;

public class ProfilePage extends BasePage {

	public ProfilePage(WebDriver driver) {
		super(driver);
		PageFactory.initElements(driver, this);
	}

	@FindBy(id = "age")
	private WebElement txtAge;

	@FindBy(xpath = "//button[text()='Save']")
	private WebElement btnSave;

	@FindBy(xpath = "//a[text()='Profile']")
	private WebElement btnProfile;

	@FindBy(xpath = "//div[@class=\"result alert alert-danger\"]")
	private WebElement lblError;
	
	@FindBy(id = "phone")
	private WebElement txtPhone;

	public void enterLetters() {
		try {
			WaitWebDriver.waitClickable(driver, txtAge, 10);
			txtAge.sendKeys("asd");
		} catch (Exception e) {
			System.out.println(e.getMessage().toString());
		}
	}

	public void pressSave() {
		try {
			WaitWebDriver.waitClickable(driver, btnSave, 10);
			btnSave.click();
			
		} catch (Exception e) {
			System.out.println(e.getMessage().toString());
		}
	}

	public boolean labelError() {
		boolean bolean = false;
		try {
			WaitWebDriver.waitSeconds(3);
			JavascriptExecutor executor = (JavascriptExecutor)driver;
			executor.executeScript("arguments[0].click();", lblError);
			bolean = true;
		} catch (Exception e) {
			System.out.println(e.getMessage().toString());
		}
		return bolean;
	}
	
	public boolean specialCharactersPhone() {
		boolean bolean = false;
		try {
			WaitWebDriver.waitClickable(driver, txtPhone, 10);
			txtPhone.clear();
			txtPhone.sendKeys("((((((((((((((");
			bolean = true;
		} catch (Exception e) {
			System.out.println(e.getMessage().toString());
		}
		return bolean;
	}
	
	

}
