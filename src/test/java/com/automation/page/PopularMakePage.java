package com.automation.page;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import com.automation.utils.WaitWebDriver;

public class PopularMakePage extends BasePage {

	public PopularMakePage(WebDriver driver) {
		super(driver);
		PageFactory.initElements(driver, this);
	}

	@FindBy(xpath = "//a[text()='Buggy Rating']")
	private WebElement btnBuggyRating;


	public void pressBuggyRating() {
		try {
			WaitWebDriver.waitClickable(driver, btnBuggyRating, 10);
			btnBuggyRating.click();
		} catch (Exception e) {
			System.out.println(e.getMessage().toString());
		}
	}
	
	
	
	

}
