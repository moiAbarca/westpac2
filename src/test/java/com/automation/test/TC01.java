package com.automation.test;

import static org.testng.Assert.assertTrue;

import org.testng.annotations.Test;

import com.automation.config.BaseConfig;
import com.automation.page.HomePage;

public class TC01 extends BaseConfig{
	@Test(description = "When you insert letters in the textbox Age, system show you the message unknown error")
	public void lettersAge() {
		HomePage homePage = new HomePage(driver);
		homePage.login();
		homePage.enterProfile();
		homePage.profilePage().enterLetters();
		homePage.profilePage().pressSave();
		assertTrue(homePage.profilePage().labelError());
		
	}

}
