package com.automation.test;

import static org.testng.Assert.assertTrue;

import org.testng.annotations.Test;

import com.automation.config.BaseConfig;
import com.automation.page.HomePage;

public class TC3 extends BaseConfig{
	@Test(description = "In the “Popular Make” page, system don’t go to the home page")
	public void textboxPhone() {
		HomePage homePage = new HomePage(driver);
		homePage.popularMake();
		homePage.popularMakePage().pressBuggyRating();
		assertTrue(homePage.validHomePage());
		
		
	}

}
