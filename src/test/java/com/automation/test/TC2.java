package com.automation.test;

import static org.testng.Assert.assertTrue;

import org.testng.annotations.Test;

import com.automation.config.BaseConfig;
import com.automation.page.HomePage;

public class TC2 extends BaseConfig{
	@Test(description = "In the texbox Phone, you can enter some special characters ")
	public void textboxPhone() {
		HomePage homePage = new HomePage(driver);
		homePage.login();
		homePage.enterProfile();
		homePage.profilePage().enterLetters();
		assertTrue(homePage.profilePage().specialCharactersPhone());
		homePage.profilePage().pressSave();
		
		
	}

}
